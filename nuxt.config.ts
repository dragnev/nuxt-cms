// https://nuxt.com/docs/api/configuration/nuxt-config

const { BASE_URL } = process.env
const base = BASE_URL && new URL(BASE_URL).pathname

export default defineNuxtConfig({
  router: {
    base
  },
  devtools: { enabled: true },
  modules: ['@nuxt/content'],
  css: ['~/assets/css/picnic.min.css', "~/assets/css/main.css"]
})

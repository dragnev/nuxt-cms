export default defineNuxtLink({
    componentName: "NavLink",
    activeClass: "active"
});